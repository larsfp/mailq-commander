# MailQ-commander

## Overview

Mailq, exim, exiqgrep are tedious for fixing mail stuck in queue. Perhapse a TUI can improve the workflow?

## Features

* View sum
* View queue
* View messages
* Mark several messages interactively, or with a filter
* Run actions on messages, like delete

### Secondary features

* Warn on panic log
* Clear panic log
* Run via ssh

## Sketches

